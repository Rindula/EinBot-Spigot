package de.einjava.tsbot.main;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by EinJava on 10.10.2017.
 * development for a Minecraft Network.
 * work with love.
 */
public class Data {

    public static HashMap<Player, String> rank = new HashMap<>();


    private static String version;
    private static String newVersion;
    public static ArrayList<Integer> clients = new ArrayList<>();
    public static HashMap<String, Integer> databaseId = new HashMap<>();
    public static HashMap<String, Integer> clientId = new HashMap<>();
    public static HashMap<Player, String> uuid = new HashMap<>();
    public static HashMap<Player, Integer> id = new HashMap<>();
    public static HashMap<Player, String> name = new HashMap<>();

    public static String getNewVersion() {
        return newVersion;
    }

    public static void setNewVersion(String newVersion) {
        Data.newVersion = newVersion;
    }

    public static String getVersion() {
        return version;
    }

    public static void setVersion(String version) {
        Data.version = version;
    }

    public static String
            MySQL_User,
            MySQL_Database,
            MySQL_Host,
            MySQL_Password,
            Query_User,
            Query_Host,
            Query_Password,
            Bot_Name,
            Support_Permission,
            ServerName,
            Bereits_Verifiziert,
            Help_update,
            Suche,
            Request,
            Request_Text,
            Hover_Accept,
            Verify_Deny,
            TeamSpeak_Verify_MSG,
            Ingame_Verify_MSG,
            Hover_Deny,
            Help_delete,
            Erfolgreiche_Suche,
            Erfolgreich_Gelöscht,
            Help_help,
            Support_Waiting,
            Client,
            Help_info,
            Support_Notify_Ingame,
            Join_Nachricht_1,
            Support_Notify_TeamSpeak,
            Join_Nachricht_2,
            Join_Nachricht_3,
            Join_Nachricht,
            TeamSpeak_Verifiziert,
            Ingame_Verifiziert,
            Nicht_Verifiziert,
            Fehlgeschlagene_Suche,
            Synchronisieren,
            Info_UUID,
            Neugeladen_Ingame,
            Neugeladen_TeamSpeak,
            Info_Rang,
            TeamSpeak_BereitsVerifiziert,
            Ingame_Verifiziert_MSG,
            Info_Database,
            Info_ID,
            Nachricht,
            Info_Client_ID,
            System,
            Verify_IP_Disable,
            Ingame_Verifiziert_Player,
            TeamSpeak_Verifiziert_MSG,
            Player_Offline,
            Player_Verifiziert,
            Default_Name;

    public static Integer
            TeamSpeak_Port,
            Admin_ID,
            Verify_ID,
            Default_ID,
            Support_ID,
            Support_Channel_ID;

    public static boolean
            Support,
            JoinNachricht,
            Beta,
            Verify;
}