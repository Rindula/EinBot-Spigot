package de.einjava.tsbot.main;

import com.github.theholywaffle.teamspeak3.TS3ApiAsync;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import de.einjava.tsbot.command.Verify;
import de.einjava.tsbot.listener.*;
import de.einjava.tsbot.utils.*;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;

/**
 * Created by EinJava on 22.09.2017.
 * development for a Minecraft Network.
 * work with love.
 */

public class Bot extends JavaPlugin {

    private TS3Config ts3Config = new TS3Config();
    private TS3Query query = new TS3Query(ts3Config);
    private TS3ApiAsync api = query.getAsyncApi();
    private Methods methods = new Methods();
    private MySQL mySQL = new MySQL();
    private static Bot instance;

    @Override
    public void onEnable() {
        instance = this;
        Data.setVersion("3.1.19");
        Config.getInstance().loadConfig();
        getMethods().sendStarting();
        if (Data.Verify) {
            connectMySQL();
        }
        connectTeamSpeak();
        register();
    }


    private void register() {
        PluginManager pm = Bukkit.getPluginManager();
        if (Data.Verify) {
            getCommand("verify").setExecutor(new Verify());
        }
        if (Data.Support) {
            ClientMovedListener.register();
        }
        ClientJoinListener.register();
        TestMessageListener.register();
        pm.registerEvents(new LoginListener(), this);
    }

    private void connectMySQL() {
        getMySQL().createTable();
    }

    private void connectTeamSpeak() {
        getTs3Config().setHost(Data.Query_Host);
        getQuery().connect();
        getApi().login(Data.Query_User, Data.Query_Password);
        getApi().selectVirtualServerByPort(Data.TeamSpeak_Port);
        getApi().setNickname(Data.Bot_Name);
        if (api != null && query != null) {
            getMethods().sendMessage("§aEine Verbindung zum TeamSpeak war erfolgreich");
        } else {
            getMethods().sendMessage("§cDie Verbindung zum TeamSpeak-Server ist fehlgeschlagen");
        }
    }

    @Override
    public void onDisable() {
        getApi().logout();
        getQuery().exit();
        getMySQL().disconnect();
    }

    public TS3Config getTs3Config() {
        return ts3Config;
    }

    public Methods getMethods() {
        return methods;
    }

    public TS3ApiAsync getApi() {
        return api;
    }

    public MySQL getMySQL() {
        return mySQL;
    }


    public TS3Query getQuery() {
        return query;
    }

    public static Bot getInstance() {
        return instance;
    }
}
