package de.einjava.tsbot.listener;

import de.einjava.tsbot.main.Bot;
import de.einjava.tsbot.main.Data;
import de.einjava.tsbot.utils.VerifyManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class LoginListener implements Listener {

    @EventHandler
    public void onLogin(PlayerJoinEvent e) {
        new Thread(() -> {
            Player player = e.getPlayer();
            Bukkit.getScheduler().scheduleAsyncDelayedTask(Bot.getInstance(), new Runnable() {
                @Override
                public void run() {
                    if (Data.Beta) {
                        Data.rank.put(player, Bot.getInstance().getMethods().getRank(player));
                    }
                    if (Data.Beta) {
                        try {
                            if (new VerifyManager(player).getTypebyUUID() == 3) {
                                Bot.getInstance().getMethods().setClientGroupsasJoin(player, Bot.getInstance().getApi().getClientByUId(new VerifyManager(player).getIDbyUUID()).getUninterruptibly().getId());
                            }
                        } catch (Exception ignored) {
                        }
                    }
                }
            });
        }).start();
    }
}