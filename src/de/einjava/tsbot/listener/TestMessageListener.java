package de.einjava.tsbot.listener;

import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventType;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import de.einjava.tsbot.main.Bot;
import de.einjava.tsbot.main.Data;
import de.einjava.tsbot.utils.VerifyManager;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class TestMessageListener {

    final static int clientid = Bot.getInstance().getApi().whoAmI().getUninterruptibly().getId();

    public static void register() {
        Bot.getInstance().getApi().registerEvent(TS3EventType.TEXT_PRIVATE, 0);
        Bot.getInstance().getApi().addTS3Listeners(new TS3EventAdapter() {

            @Override
            public void onTextMessage(TextMessageEvent e) {
                if (e.getTargetMode() == TextMessageTargetMode.CLIENT && e.getInvokerId() != clientid) {
                    String msg = e.getMessage();
                    Client client = Bot.getInstance().getApi().getClientByUId(e.getInvokerUniqueId()).getUninterruptibly();
                    if (Data.System.equalsIgnoreCase("Schreiben")) {
                        if (msg.startsWith("!verify")) {
                            if (new VerifyManager(e.getInvokerUniqueId()).isExistPlayer()) {
                                if (new VerifyManager(e.getInvokerUniqueId()).getTypebyID() == 3) {
                                    Bot.getInstance().getApi().sendPrivateMessage(client.getId(), Data.TeamSpeak_BereitsVerifiziert);
                                    return;
                                }
                            }
                            try {
                                String name = e.getMessage().split(" ")[1];
                                Player player = Bukkit.getPlayer(name);
                                if (player == null) {
                                    Bot.getInstance().getApi().sendPrivateMessage(client.getId(), Data.Player_Offline.replace("%name%", name));
                                } else if (new VerifyManager(player).getTypebyUUID() == 3) {
                                    Bot.getInstance().getApi().sendPrivateMessage(client.getId(), Data.Player_Verifiziert);
                                } else {
                                    Bot.getInstance().getApi().sendPrivateMessage(client.getId(), "» Es wurde Ingame eine Anfrage versendet");
                                    VerifyManager verifyManager = new VerifyManager(player);
                                    verifyManager.createPlayerIngame();
                                    verifyManager.setTypebyUUID(2);
                                    Data.uuid.put(player, e.getInvokerUniqueId());
                                    Data.id.put(player, client.getId());
                                    Data.name.put(player, client.getNickname());
                                    player.sendMessage(Data.Request.replace("%client%", client.getNickname()));
                                    sendRequest(player);
                                }
                            } catch (ArrayIndexOutOfBoundsException ignored) {
                                Bot.getInstance().getApi().sendPrivateMessage(client.getId(), "» Bitte nutze !verify <name>");
                            }
                        }
                    }
                }
            }
        });
    }

    private static void sendRequest(Player player) {
        TextComponent accept = new TextComponent(Data.Hover_Accept);
        accept.setHoverEvent(
                new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                        new ComponentBuilder(Data.Hover_Accept)
                                .create()));
        accept.setClickEvent(
                new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/verify accept"));

        TextComponent deny = new TextComponent(Data.Hover_Deny);
        deny.setHoverEvent(
                new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                        new ComponentBuilder(Data.Hover_Deny)
                                .create()));
        deny.setClickEvent(
                new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/verify deny"));

        TextComponent txt = new TextComponent(" §8┃§r ");
        TextComponent msg = new TextComponent(Data.Request_Text);
        msg.addExtra(accept);
        msg.addExtra(txt);
        msg.addExtra(deny);
        player.spigot().sendMessage(msg);
    }
}
