package de.einjava.tsbot.listener;

import com.github.theholywaffle.teamspeak3.api.event.ClientMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventType;
import de.einjava.tsbot.main.Bot;
import de.einjava.tsbot.main.Data;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * Created by EinJava on 04.01.2018.
 * development with love.
 */
public class ClientMovedListener {

    public static void register() {
        Bot.getInstance().getApi().registerEvent(TS3EventType.CHANNEL, 0);
        Bot.getInstance().getApi().addTS3Listeners(new TS3EventAdapter() {
            @Override
            public void onClientMoved(ClientMovedEvent e) {
                if (e.getTargetChannelId() == Data.Support_Channel_ID) {
                    String name = Bot.getInstance().getApi().getClientInfo(e.getClientId()).getUninterruptibly().getNickname();
                    String channel = Bot.getInstance().getApi().getChannelInfo(e.getTargetChannelId()).getUninterruptibly().getName();
                    for (Integer clientId : Data.clients) {
                        if (Data.Nachricht.equalsIgnoreCase("Poke")) {
                            Bot.getInstance().getApi().pokeClient(clientId, Data.Support_Notify_TeamSpeak.replace("%client%", name).replace("%channel%", channel));
                        } else if (Data.Nachricht.equalsIgnoreCase("MSG")) {
                            Bot.getInstance().getApi().sendPrivateMessage(clientId, Data.Support_Notify_TeamSpeak.replace("%client%", name).replace("%channel%", channel));
                        } else {
                            Bot.getInstance().getApi().pokeClient(clientId, "Du kannst nur 'Poke' oder 'MSG' als Nachricht verwenden!");
                        }
                    }
                    for (Player a : Bukkit.getOnlinePlayers()) {
                        if (a.hasPermission(Data.Support_Permission)) {
                            a.sendMessage(Data.Support_Notify_Ingame.replace("%client%", name).replace("%channel%", channel));
                        }
                    }
                    Bot.getInstance().getApi().sendPrivateMessage(e.getClientId(), Data.Support_Waiting);
                }
            }
        });
    }
}