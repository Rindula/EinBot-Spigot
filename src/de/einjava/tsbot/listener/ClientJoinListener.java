package de.einjava.tsbot.listener;

import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.api.event.ClientJoinEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventType;
import de.einjava.tsbot.main.Bot;
import de.einjava.tsbot.main.Data;
import de.einjava.tsbot.utils.VerifyManager;

import javax.imageio.ImageIO;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;

/**
 * Created by EinJava on 23.12.2017.
 * development with love.
 */
public class ClientJoinListener {

    public static void register() {
        Bot.getInstance().getApi().registerEvent(TS3EventType.SERVER, 0);
        Bot.getInstance().getApi().addTS3Listeners(new TS3EventAdapter() {
            @Override
            public void onClientJoin(ClientJoinEvent e) {
                VerifyManager verifyManager = new VerifyManager(e.getUniqueClientIdentifier());
                if (Bot.getInstance().getMethods().getClientInfo(e.getClientId()).isInServerGroup(Data.Support_ID)) {
                    if (!Data.clients.contains(e.getClientId())) {
                        Data.clients.add(e.getClientId());
                    }
                }
                Data.clientId.put(e.getUniqueClientIdentifier(), e.getClientId());
                Data.databaseId.put(e.getUniqueClientIdentifier(), e.getClientDatabaseId());
                if (verifyManager.getTypebyID() == 3) {
                    Bot.getInstance().getApi().addClientToServerGroup(Data.Verify_ID, e.getClientDatabaseId());
                    if (Data.JoinNachricht) {
                        Bot.getInstance().getApi().sendPrivateMessage(e.getClientId(), Data.Join_Nachricht.replace("%client%", e.getClientNickname()).replace("%server%", Data.ServerName));
                    }
                } else {
                    Bot.getInstance().getApi().removeClientFromServerGroup(Data.Verify_ID, e.getClientDatabaseId());
                    Bot.getInstance().getApi().editClient(e.getClientId(), Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION, "Unbekannt"));
                    Bot.getInstance().getApi().sendPrivateMessage(e.getClientId(), Data.Join_Nachricht_1);
                    Bot.getInstance().getApi().sendPrivateMessage(e.getClientId(), Data.Join_Nachricht_2.replace("%server%", Data.ServerName));
                    Bot.getInstance().getApi().sendPrivateMessage(e.getClientId(), Data.Join_Nachricht_3);
                }
            }
        });
    }
}
