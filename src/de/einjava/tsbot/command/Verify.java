package de.einjava.tsbot.command;

import com.github.theholywaffle.teamspeak3.api.ClientProperty;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import de.einjava.tsbot.main.Bot;
import de.einjava.tsbot.main.Data;
import de.einjava.tsbot.utils.VerifyManager;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import java.util.Collections;

public class Verify implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        try {
            Player p = (Player) sender;
            VerifyManager verifyManager = new VerifyManager(p);
            verifyManager.createPlayerIngame();
            int id = 0;
            boolean check = false;
            if (args.length == 0) {
                if (Data.System.equalsIgnoreCase("IP")) {
                    if (verifyManager.getTypebyUUID() == 3) {
                        p.sendMessage(Data.Bereits_Verifiziert);
                    } else {
                        p.sendMessage(Data.Suche);
                        for (Client c : Bot.getInstance().getApi().getClients().getUninterruptibly()) {
                            if (c.getIp().equalsIgnoreCase(p.getAddress().getHostString())) {
                                check = true;
                                p.sendMessage(Data.Erfolgreiche_Suche);
                                p.sendMessage(Data.Client.replace("%client%", c.getNickname()));
                                id = c.getId();
                                verifyManager.setIDbyUUID(c.getUniqueIdentifier());
                                verifyManager.setTypebyUUID(3);
                                Bot.getInstance().getApi().editClient(id, Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION, p.getName() + " | " + p.getUniqueId()));
                                Bot.getInstance().getApi().sendPrivateMessage(id, Data.TeamSpeak_Verifiziert);
                                p.sendMessage(Data.Ingame_Verifiziert);
                                p.sendMessage(Data.Synchronisieren);
                                Bot.getInstance().getMethods().setClientGroups(p, id);
                                Bot.getInstance().getApi().editClient(id, Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION, "Name: " + p.getName() + " | UUID: " + p.getUniqueId()));
                            }
                        }
                        if (!check) {
                            p.sendMessage(Data.Fehlgeschlagene_Suche);
                        }
                    }
                } else {
                    p.sendMessage(Data.Verify_IP_Disable);
                }
            } else if (args.length == 1) {
                if (args[0].equalsIgnoreCase("update")) {
                    if (verifyManager.getTypebyUUID() == 3) {
                        if (Data.System.equalsIgnoreCase("IP")) {
                            for (Client c : Bot.getInstance().getApi().getClients().getUninterruptibly()) {
                                if (c.getIp().equalsIgnoreCase(p.getAddress().getHostString())) {
                                    check = true;
                                    id = c.getId();
                                    p.sendMessage(Data.Synchronisieren);
                                    Bot.getInstance().getMethods().setClientGroups(p, id);
                                    Bot.getInstance().getApi().editClient(id, Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION, "Name: " + p.getName() + " | UUID: " + p.getUniqueId()));
                                }
                            }
                            if (!check) {
                                p.sendMessage(Data.Fehlgeschlagene_Suche);
                            }
                        } else {
                            id = Bot.getInstance().getApi().getClientByUId(new VerifyManager(p).getIDbyUUID()).getUninterruptibly().getId();
                            p.sendMessage(Data.Synchronisieren);
                            Bot.getInstance().getMethods().setClientGroups(p, id);
                            Bot.getInstance().getApi().editClient(id, Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION, "Name: " + p.getName() + " | UUID: " + p.getUniqueId()));
                        }
                    } else {
                        p.sendMessage(Data.Nicht_Verifiziert);
                    }
                }
                if (args[0].equalsIgnoreCase("info")) {
                    if (verifyManager.getTypebyUUID() == 3) {
                        String uuid = verifyManager.getIDbyUUID();
                        p.sendMessage(Bot.getInstance().getMethods().getPrefix() + "§7Spieler §8» §e" + p.getName());
                        p.sendMessage(Data.Info_UUID.replace("%uuid%", p.getUniqueId().toString()));
                        p.sendMessage(Data.Info_Rang.replace("%rank%", verifyManager.getRankbyUUID()));
                        p.sendMessage(Data.Info_ID.replace("%id%", uuid));
                        if (Data.clientId.containsKey(uuid)) {
                            p.sendMessage(Data.Info_Client_ID.replace("%client-id%", String.valueOf(Data.clientId.get(uuid))));
                        }
                        if (Data.databaseId.containsKey(uuid)) {
                            p.sendMessage(Data.Info_Database.replace("%database%", String.valueOf(Data.databaseId.get(uuid))));
                        }
                    } else {
                        p.sendMessage(Data.Nicht_Verifiziert);
                    }
                }
                if (args[0].equalsIgnoreCase("delete")) {
                    if (verifyManager.getTypebyUUID() == 3) {
                        verifyManager.delete();
                        p.sendMessage(Data.Erfolgreich_Gelöscht);
                    } else {
                        p.sendMessage(Data.Nicht_Verifiziert);
                    }
                }
                if (args[0].equalsIgnoreCase("accept")) {
                    if (Data.name.containsKey(p)) {
                        p.sendMessage(Data.Ingame_Verifiziert_MSG.replace("%client%", Data.name.get(p)));
                        Bot.getInstance().getApi().sendPrivateMessage(Data.id.get(p), Data.TeamSpeak_Verifiziert_MSG.replace("%name%", p.getName()));
                        verifyManager.setTypebyUUID(3);
                        verifyManager.setIDbyUUID(Data.uuid.get(p));
                        p.sendMessage(Data.Synchronisieren);
                        Bot.getInstance().getMethods().setClientGroups(p, Data.id.get(p));
                        Bot.getInstance().getApi().editClient(Data.id.get(p), Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION, "Name: " + p.getName() + " | UUID: " + p.getUniqueId()));
                        Data.name.remove(p);
                    } else {
                        p.sendMessage(Data.Nicht_Verifiziert);
                    }
                }
                if (args[0].equalsIgnoreCase("deny")) {
                    if (Data.name.containsKey(p)) {
                        verifyManager.setTypebyUUID(0);
                        p.sendMessage(Data.Verify_Deny.replace("%client%", Data.name.get(p)));
                        Bot.getInstance().getApi().sendPrivateMessage(Data.id.get(p), "» " + p.getName() + " hat deine Anfrage abgehlehnt!");
                        Data.name.remove(p);
                    } else {
                        p.sendMessage(Data.Nicht_Verifiziert);
                    }
                }
                if (args[0].equalsIgnoreCase("help")) {
                    sendHelp(p);
                }
            } else if (args.length == 2) {
                if (args[0].equalsIgnoreCase("update")) {
                    Player target = Bukkit.getPlayer(args[1]);
                    VerifyManager verify = new VerifyManager(target);
                    verify.createPlayerIngame();
                    if (verify.getTypebyUUID() == 3) {
                        if (Data.System.equalsIgnoreCase("IP")) {
                            for (Client c : Bot.getInstance().getApi().getClients().getUninterruptibly()) {
                                if (c.getIp().equalsIgnoreCase(target.getAddress().getHostString())) {
                                    check = true;
                                    id = c.getId();
                                    target.sendMessage(Data.Synchronisieren);
                                    Bot.getInstance().getMethods().setClientGroups(target, id);
                                    Bot.getInstance().getApi().editClient(id, Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION, "Name: " + target.getName() + " | UUID: " + target.getUniqueId()));
                                    p.sendMessage(Data.Ingame_Verifiziert_Player.replace("%client%", target.getDisplayName()));
                                }
                            }
                            if (!check) {
                                target.sendMessage(Data.Fehlgeschlagene_Suche);
                            }
                        } else {
                            id = Bot.getInstance().getApi().getClientByUId(new VerifyManager(p).getIDbyUUID()).getUninterruptibly().getId();
                            p.sendMessage(Data.Synchronisieren);
                            Bot.getInstance().getMethods().setClientGroups(p, id);
                            Bot.getInstance().getApi().editClient(id, Collections.singletonMap(ClientProperty.CLIENT_DESCRIPTION, "Name: " + p.getName() + " | UUID: " + p.getUniqueId()));
                        }
                    } else {
                        target.sendMessage(Data.Nicht_Verifiziert);
                    }
                }
            } else {
                sendHelp(p);
            }

        } catch (Exception ignored) {

        }
        return false;
    }

    private void sendHelp(Player p) {
        p.sendMessage(Data.Help_update);
        p.sendMessage(Data.Help_info);
        p.sendMessage(Data.Help_delete);
        p.sendMessage(Data.Help_help);
    }
}
