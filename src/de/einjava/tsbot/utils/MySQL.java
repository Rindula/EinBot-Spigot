package de.einjava.tsbot.utils;

import de.einjava.tsbot.main.Bot;
import de.einjava.tsbot.main.Data;

import java.sql.*;

/**
 * Created by EinJava on 22.12.2017.
 * development with love.
 */
public class MySQL {

    private Connection connection;

    private void connect( ) {
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + Data.MySQL_Host + ":3306/" + Data.MySQL_Database + "?autoReconnect=true", Data.MySQL_User, Data.MySQL_Password);
            Bot.getInstance().getMethods().sendMessage("§aEine Verbindung zur MySQL war erfolgreich");
        } catch (SQLException e) {
            Bot.getInstance().getMethods().sendMessage("§cDie Verbindung zum MySQL-Server ist fehlgeschlagen");
        }
    }

    public void createTable( ) {
        connect();
        update("CREATE TABLE IF NOT EXISTS verify (UUID varchar(64), NAME varchar(64), RANK varchar(64), TYPE int, ID varchar(64));");
    }

    public void disconnect( ) {
        if (isConnected()) {
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private boolean isConnected( ) {
        return connection != null;
    }

    public Connection getConnection( ) {
        return connection;
    }

    public void update( String qry ) {
        try {
            PreparedStatement ps = connection.prepareStatement(qry);
            ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet query( String qry ) {
        try {
            PreparedStatement ps = connection.prepareStatement(qry);
            return ps.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}