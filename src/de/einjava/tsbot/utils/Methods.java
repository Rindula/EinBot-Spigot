package de.einjava.tsbot.utils;

import com.github.theholywaffle.teamspeak3.api.wrapper.ClientInfo;
import com.github.theholywaffle.teamspeak3.api.wrapper.ServerGroup;
import de.einjava.tsbot.main.Bot;
import de.einjava.tsbot.main.Data;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created by EinJava on 22.12.2017.
 * development with love.
 */
public class Methods {

    private String prefix = "";

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public void sendStarting() {
        Bukkit.getConsoleSender().sendMessage("§f    _______       ____        __ \n" +
                " _______ _       ______             \n" +
                "(_______|_)     (____  \\        _   \n" +
                " _____   _ ____  ____)  ) ___ _| |_ \n" +
                "|  ___) | |  _ \\|  __  ( / _ (_   _)\n" +
                "| |_____| | | | | |__)  ) |_| || |_ \n" +
                "|_______)_|_| |_|______/ \\___/  \\__)\n" +
                "                                    \n");
        Bukkit.getConsoleSender().sendMessage("");
        sendMessage("development by Papiertuch | Version: " + Data.getVersion());
        sendMessage("Loading Config...");
        sendMessage("Loading Modules...");
        sendMessage("Loading Database...");
        sendMessage("Verify: [" + Data.Verify + "] [" + Data.System + "]");
        sendMessage("Support: [" + Data.Support + "] [" + Data.Nachricht + "]");
    }

    public String getHostAddress() {
        String address = "";
        try {
            InetAddress inetAddress = InetAddress.getLocalHost();
            address = inetAddress.getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return address;
    }

    public ClientInfo getClientInfo(int id) {
        return Bot.getInstance().getApi().getClientInfo(id).getUninterruptibly();
    }

    public void setClientGroups(Player player, int id) {
        new Thread(() -> {
            Bukkit.getScheduler().scheduleAsyncDelayedTask(Bot.getInstance(), new Runnable() {
                @Override
                public void run() {
                    String groupName;
                    int rankID;
                    String rank = "";
                    rank = getRank(player);
                    new VerifyManager(player).setRankbyUUID(rank);
                    int databaseID = getClientInfo(id).getDatabaseId();
                    for (ServerGroup serverGroup : Bot.getInstance().getApi().getServerGroups().getUninterruptibly()) {
                        groupName = serverGroup.getName();
                        rankID = serverGroup.getId();
                        if (groupName.equalsIgnoreCase(rank)) {
                            Bot.getInstance().getApi().addClientToServerGroup(rankID, databaseID);
                            Bot.getInstance().getApi().addClientToServerGroup(Data.Verify_ID, databaseID);
                        }
                        if (!groupName.equalsIgnoreCase(rank) && rankID != Data.Verify_ID) {
                            for (int i = 0; i < Config.ranklist.size(); i++) {
                                if (Config.ranklist.get(i).split("; ")[0].equalsIgnoreCase(groupName)) {
                                    Bot.getInstance().getApi().removeClientFromServerGroup(rankID, databaseID);
                                }
                            }
                        }
                    }
                    Bot.getInstance().getApi().sendPrivateMessage(id, Data.Neugeladen_TeamSpeak);
                    player.sendMessage(Data.Neugeladen_Ingame);
                }
            });
        }).start();
    }

    public void setClientGroupsasJoin(Player player, int id) {
        new Thread(() -> {
            Bukkit.getScheduler().scheduleAsyncDelayedTask(Bot.getInstance(), new Runnable() {
                @Override
                public void run() {
                    String groupName;
                    int rankID;
                    String rank = "";
                    rank = Data.rank.get(player);
                    new VerifyManager(player).setRankbyUUID(rank);
                    int databaseID = getClientInfo(id).getDatabaseId();
                    for (ServerGroup serverGroup : Bot.getInstance().getApi().getServerGroups().getUninterruptibly()) {
                        groupName = serverGroup.getName();
                        rankID = serverGroup.getId();
                        if (groupName.equalsIgnoreCase(rank)) {
                            Bot.getInstance().getApi().addClientToServerGroup(rankID, databaseID);
                            Bot.getInstance().getApi().addClientToServerGroup(Data.Verify_ID, databaseID);
                        }
                        if (!groupName.equalsIgnoreCase(rank) && rankID != Data.Verify_ID) {
                            for (int i = 0; i < Config.ranklist.size(); i++) {
                                if (Config.ranklist.get(i).split("; ")[0].equalsIgnoreCase(groupName)) {
                                    Bot.getInstance().getApi().removeClientFromServerGroup(rankID, databaseID);
                                }
                            }
                        }
                    }
                }
            });
        }).start();
    }


    public String getRank(Player p) {
        String rank = "";
        for (int i = 0; i < Config.ranklist.size(); i++) {
            if (p.hasPermission(Config.ranklist.get(i).split("; ")[1])) {
                rank = Config.ranklist.get(i).split("; ")[0];
                break;
            }
        }
        return rank;
    }

    public String getPrefix() {
        return prefix;
    }

    public void sendMessage(String message) {
        Bukkit.getConsoleSender().sendMessage("EinBot > " + message);
    }
}