package de.einjava.tsbot.utils;

import de.einjava.tsbot.main.Bot;
import de.einjava.tsbot.main.Data;
import org.bukkit.entity.Player;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Consumer;

/**
 * Created by EinJava on 22.12.2017.
 * development with love.
 */
public class VerifyManager {

    private static UUID uuid;
    private static String clientid;
    private static Player player;


    public VerifyManager(String id) {
        clientid = id;
    }

    public VerifyManager(Player proxiedPlayer) {
        player = proxiedPlayer;
        uuid = proxiedPlayer.getUniqueId();
    }

    public void delete() {
        Bot.getInstance().getMySQL().update("DELETE FROM verify WHERE UUID = '" + uuid + "'");
    }


    public String getRankbyUUID() {
        if (isExistPlayerIngame()) {
            try {
                PreparedStatement preparedStatement = Bot.getInstance().getMySQL().getConnection().prepareStatement("SELECT * FROM verify WHERE UUID = ?");
                preparedStatement.setString(1, uuid.toString());
                ResultSet rs = preparedStatement.executeQuery();
                if (rs.next()) {
                    return (rs.getString("RANK"));
                }
                rs.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

/*
    public void get() {
        new VerifyManager(player).getRankbyUUID(rank -> {
            player.sendMessage("Dein Rang >" + rank);
        });
    }
*/
    public void setIDbyUUID(String id) {
        if (isExistPlayerIngame()) {
            try {
                PreparedStatement preparedStatement = Bot.getInstance().getMySQL().getConnection().prepareStatement("UPDATE verify SET ID = ? WHERE UUID = ?");
                preparedStatement.setString(1, id);
                preparedStatement.setString(2, uuid.toString());
                preparedStatement.executeUpdate();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            createPlayerIngame();
            setIDbyUUID(id);
        }
    }


    public void setTypebyUUID(Integer type) {
        if (isExistPlayerIngame()) {
            try {
                PreparedStatement preparedStatement = Bot.getInstance().getMySQL().getConnection().prepareStatement("UPDATE verify SET TYPE = ? WHERE UUID = ?");
                preparedStatement.setString(2, uuid.toString());
                preparedStatement.setInt(1, type);
                preparedStatement.executeUpdate();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            createPlayerIngame();
            setTypebyUUID(type);
        }
    }


    public void setRankbyUUID(String rank) {
        if (isExistPlayerIngame()) {
            try {
                PreparedStatement preparedStatement = Bot.getInstance().getMySQL().getConnection().prepareStatement("UPDATE verify SET RANK = ? WHERE UUID = ?");
                preparedStatement.setString(2, uuid.toString());
                preparedStatement.setString(1, rank);
                preparedStatement.executeUpdate();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            createPlayerIngame();
            setRankbyUUID(rank);
        }
    }

    public void createPlayerIngame() {
        if (!isExistPlayerIngame()) {
            try {
                PreparedStatement preparedStatement = Bot.getInstance().getMySQL().getConnection().prepareStatement("INSERT INTO verify (UUID, NAME, RANK, TYPE, ID) VALUES (?, ?, ?, ?, ?)");
                preparedStatement.setString(1, uuid.toString());
                preparedStatement.setString(2, player.getName());
                preparedStatement.setString(3, Data.Default_Name);
                preparedStatement.setInt(4, 0);
                preparedStatement.setString(5, null);
                preparedStatement.executeUpdate();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public String getRankbyID() {
        if (isExistPlayer()) {
            try {
                PreparedStatement preparedStatement = Bot.getInstance().getMySQL().getConnection().prepareStatement("SELECT * FROM verify WHERE ID = ?");
                preparedStatement.setString(1, clientid);
                ResultSet rs = preparedStatement.executeQuery();
                if (rs.next()) {
                    return rs.getString("RANK");
                }
                rs.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public Integer getTypebyID() {
        if (isExistPlayer()) {
            try {
                PreparedStatement preparedStatement = Bot.getInstance().getMySQL().getConnection().prepareStatement("SELECT * FROM verify WHERE ID = ?");
                preparedStatement.setString(1, clientid);
                ResultSet rs = preparedStatement.executeQuery();
                if (rs.next()) {
                    return rs.getInt("TYPE");
                }
                rs.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            return 0;
        }
        return 0;
    }

    public Integer getTypebyUUID() {
        if (isExistPlayerIngame()) {
            try {
                PreparedStatement preparedStatement = Bot.getInstance().getMySQL().getConnection().prepareStatement("SELECT * FROM verify WHERE UUID = ?");
                preparedStatement.setString(1, uuid.toString());
                ResultSet rs = preparedStatement.executeQuery();
                if (rs.next()) {
                    return rs.getInt("TYPE");
                }
                rs.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }

    public String getIDbyUUID() {
        if (isExistPlayerIngame()) {
            try {
                PreparedStatement preparedStatement = Bot.getInstance().getMySQL().getConnection().prepareStatement("SELECT * FROM verify WHERE UUID = ?");
                preparedStatement.setString(1, uuid.toString());
                ResultSet rs = preparedStatement.executeQuery();
                if (rs.next()) {
                    return rs.getString("ID");
                }
                rs.close();
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    public boolean isExistPlayer() {
        try {
            PreparedStatement preparedStatement = Bot.getInstance().getMySQL().getConnection().prepareStatement("SELECT * FROM verify WHERE ID = ?");
            preparedStatement.setString(1, clientid);
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                if (rs.getString("ID") != null) {
                    return true;
                }
                return false;
            }
            rs.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean isExistPlayerIngame() {
        try {
            PreparedStatement preparedStatement = Bot.getInstance().getMySQL().getConnection().prepareStatement("SELECT * FROM verify WHERE UUID = ?");
            preparedStatement.setString(1, uuid.toString());
            ResultSet rs = preparedStatement.executeQuery();
            if (rs.next()) {
                if (rs.getString("UUID") != null) {
                    return true;
                }
                return false;
            }
            rs.close();
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
