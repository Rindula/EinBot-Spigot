package de.einjava.tsbot.utils;

import de.einjava.tsbot.main.Bot;
import de.einjava.tsbot.main.Data;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by EinJava on 11.10.2017.
 * development for a Minecraft Network.
 * work with love.
 */
public class Config {

    public static Config instance = new Config();
    public static ArrayList<String> ranks = new ArrayList<>();
    public static List<String> ranklist;

    public void loadConfig() {
        setConfig();
        setMessages();
        setRanks();
        getConfig();
        getMessages();
    }

    private void setConfig() {
        try {
            if (!Bot.getInstance().getDataFolder().exists()) {
                Bot.getInstance().getDataFolder().mkdir();
            }
            File file = new File(Bot.getInstance().getDataFolder().getPath(), "config.yml");
            if (!file.exists()) {
                Files.copy(Bot.getInstance().getResource("config.yml"), file.toPath(), new CopyOption[0]);
            }
        } catch (IOException localIOException1) {
        }
    }

    private void setMessages() {
        try {
            if (!Bot.getInstance().getDataFolder().exists()) {
                Bot.getInstance().getDataFolder().mkdir();
            }
            File file = new File(Bot.getInstance().getDataFolder().getPath(), "messages.yml");
            if (!file.exists()) {
                Files.copy(Bot.getInstance().getResource("messages.yml"), file.toPath(), new CopyOption[0]);
            }
        } catch (IOException localIOException1) {
        }
    }

    private void setRanks() {
        if (!Bot.getInstance().getDataFolder().exists()) {
            Bot.getInstance().getDataFolder().mkdir();
        }
        try {
            File file = new File(Bot.getInstance().getDataFolder().getPath(), "ranks.yml");
            if (!file.exists()) {
                file.createNewFile();
                FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
                ranks.add("Admin; einbot.admin");
                ranks.add("Developer; einbot.dev");
                ranks.add("SrModerator; einbot.srmod");
                ranks.add("Moderator; einbot.mod");
                ranks.add("Supporter; einbot.sup");
                ranks.add("YouTuber; einbot.youtuber");
                ranks.add("Premium; einbot.premium");
                ranks.add("Spieler; einbot.default");
                cfg.set("Ranks", ranks);
                cfg.save(file);
            }
            FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
            ranklist = cfg.getStringList("Ranks");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void getConfig() {
        File file = new File(Bot.getInstance().getDataFolder().getPath(), "config.yml");
        Configuration cfg;
        try {
            cfg = YamlConfiguration.loadConfiguration(file);
            Data.MySQL_Host = cfg.getString("MySQL_Host");
            Data.MySQL_User = cfg.getString("MySQL_User");
            Data.MySQL_Password = cfg.getString("MySQL_Password");
            Data.MySQL_Database = cfg.getString("MySQL_Database");

            Data.Query_Host = cfg.getString("Query_Host");
            Data.Query_User = cfg.getString("Query_User");
            Data.Query_Password = cfg.getString("Query_Password");
            Data.TeamSpeak_Port = cfg.getInt("TeamSpeak_Port");

            Bot.getInstance().getMethods().setPrefix(cfg.getString("Prefix").replace("&", "§"));
            Data.Bot_Name = cfg.getString("Name");
            Data.ServerName = cfg.getString("ServerName");

            Data.Verify = cfg.getBoolean("Verify");
            Data.Support = cfg.getBoolean("Support");
            Data.Admin_ID = cfg.getInt("Admin_ID");
            Data.Beta = cfg.getBoolean("Beta");
            Data.System = cfg.getString("System");

            Data.Default_ID = cfg.getInt("Default_ID");
            Data.Default_Name = cfg.getString("Default_Name");
            Data.Verify_ID = cfg.getInt("Verify_ID");
            Data.Nachricht = cfg.getString("Nachricht");
            Data.JoinNachricht = cfg.getBoolean("JoinNachricht");

            Data.Support_Permission = cfg.getString("Support_Permission");
            Data.Support_ID = cfg.getInt("Support_ID");
            Data.Support_Channel_ID = cfg.getInt("Support_Channel_ID");


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void getMessages() {
        File file = new File(Bot.getInstance().getDataFolder().getPath(), "messages.yml");
        Configuration cfg;
        try {
            cfg = YamlConfiguration.loadConfiguration(file);
            Data.Hover_Accept = cfg.getString("Hover_Accept").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Hover_Deny = cfg.getString("Hover_Deny").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Request = cfg.getString("Request").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Request_Text = cfg.getString("Request_Text").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Verify_Deny = cfg.getString("Verify_Deny").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Ingame_Verifiziert_MSG = cfg.getString("Ingame_Verifiziert_MSG").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.TeamSpeak_Verifiziert_MSG = cfg.getString("TeamSpeak_Verifiziert_MSG").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Player_Verifiziert = cfg.getString("Player_Verifiziert").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Player_Offline = cfg.getString("Player_Offline").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.TeamSpeak_BereitsVerifiziert = cfg.getString("TeamSpeak_BereitsVerifiziert").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Ingame_Verifiziert_Player = cfg.getString("Ingame_Verifiziert_Player").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Verify_IP_Disable = cfg.getString("Verify_IP_Disable").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Bereits_Verifiziert = cfg.getString("Bereits_Verifiziert").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Suche = cfg.getString("Suche").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Erfolgreiche_Suche = cfg.getString("Erfolgreiche_Suche").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Client = cfg.getString("Client").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.TeamSpeak_Verifiziert = cfg.getString("TeamSpeak_Verifiziert").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Ingame_Verifiziert = cfg.getString("Ingame_Verifiziert").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Fehlgeschlagene_Suche = cfg.getString("Fehlgeschlagene_Suche").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Synchronisieren = cfg.getString("Synchronisieren").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Info_UUID = cfg.getString("Info_UUID").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Info_Rang = cfg.getString("Info_Rang").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Info_ID = cfg.getString("Info_ID").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Info_Client_ID = cfg.getString("Info_Client_ID").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Info_Database = cfg.getString("Info_Database").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Nicht_Verifiziert = cfg.getString("Nicht_Verifiziert").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Erfolgreich_Gelöscht = cfg.getString("Erfolgreich_Geloscht").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Help_update = cfg.getString("Help_update").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Help_info = cfg.getString("Help_info").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Help_delete = cfg.getString("Help_delete").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Help_help = cfg.getString("Help_help").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Join_Nachricht_1 = cfg.getString("Join_Nachricht_1").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Join_Nachricht_2 = cfg.getString("Join_Nachricht_2").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Join_Nachricht_3 = cfg.getString("Join_Nachricht_3").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Support_Notify_TeamSpeak = cfg.getString("Support_Notify_TeamSpeak").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Support_Notify_Ingame = cfg.getString("Support_Notify_Ingame").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Support_Waiting = cfg.getString("Support_Waiting").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Neugeladen_Ingame = cfg.getString("Neugeladen_Ingame").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Neugeladen_TeamSpeak = cfg.getString("Neugeladen_TeamSpeak").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());
            Data.Join_Nachricht = cfg.getString("Join_Nachricht").replace("&", "§").replace("%prefix%", Bot.getInstance().getMethods().getPrefix());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Config getInstance() {
        return instance;
    }
}
